﻿/*
 * A portable bitmap is one of the oldest image formats around and grants access to very simple image creation and sharing. 
 * Today, you will be creating an image of this format.
 * A simple PBM program can be seen here (Note that we'll be creating the simplest version, a PBM, not PPM or PGM.)
 * But basically the program consists of the following:
 * A 2byte string (usually 'P1') denoting the file format for that PBM
 * 2 integers denoting the Width and Height of our image file respectively
 * And finally, our pixel data - Whether a pixel is 1 - Black or 0 - White.
 */

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Challenge_172_Easy_PBM
{
    class Program
    {
        const int letterWidth = 5;
        const int letterHeight = 7;

        static void Main(string[] args)
        {
            Console.WriteLine("Please enter a word for image.");
            String word = Console.ReadLine().ToUpper();

            writePBM(word, word + ".PBM");
            // Read the file and display it line by line.            
            Console.ReadLine();
            
        }

        static String hex2letter(String s)
        {
            string binaryval = "";
            string tmpBinary = "";
            for (int i = s.Length - 1; i >= 0; i--)
            {
                tmpBinary = Convert.ToString(Convert.ToInt32(s[i]+"", 16), 2);

                int rest = tmpBinary.Length % 4;
                if (rest != 0)
                {
                    tmpBinary = new string('0', 4 - rest) + tmpBinary; //pad the length out to by divideable by 4
                }
                binaryval = tmpBinary + binaryval;
            }
            
            
            return binaryval.Substring(1);
        }

        static String letter2hex(String s)
        {
            int rest = s.Length % 4;
            if (rest != 0)
            {
                s = new string('0', 4 - rest) + s; //pad the length out to by divideable by 4
            }
            
            string hex = "";

            for(int i = 0; i <= s.Length - 4; i +=4)
            {
                hex += string.Format("{0:X}", Convert.ToByte(s.Substring(i, 4), 2));
            }
            return hex;
        }

        static String[] formatLetter(String s){
            String[] letter = new String[7];
            String tmpLetter = "";
            for(int i= 0; i <7; i++){
                foreach (Char c in s.Substring(i * 5, 5))
                {                    
                    tmpLetter += c + " ";
                }
                letter[i] = tmpLetter;
                tmpLetter = "";
                //letter[i] = s.Substring(i*5,5);
            }
            return letter;
        }

        /*
         * filename:    path to file where letters are stored
         * format:      format in wich letters are stored:
         *                  B - binary
         *                  H - hexadecimal
         *                  
         * return: returns a dictionary with letters for key and array of string for values
         * 
         * Letters should be 5px wide and 7px heigh!!!
         */ 
        static Dictionary<String, String[]> readLetters(String filename, Char format)
        {
            var letters = new Dictionary<String, String[]>();
            switch (format)
            {
                case 'B':
                    {

                        String lastLetter = "";
                        String line;
                        System.IO.StreamReader file = new System.IO.StreamReader(filename);
                        while ((line = file.ReadLine()) != null)
                        {
                            //Console.WriteLine(line);
                            if (line[0].Equals('0') || line[0].Equals('1'))
                            {
                                letters.Add(lastLetter, formatLetter(line));
                                //Console.WriteLine(letter2hex(line) + " - C");
                                //Console.WriteLine(hex2letter(letter2hex(line)) + " - C");
                            }
                            else
                            {
                                lastLetter = line;
                            }
                        }
                        file.Close();
                        return letters;
                    }
                case 'H':
                    {
                        return letters;
                    }
            }
            return letters;
        }

        static void writePBM(string word, string filename){
            using (System.IO.StreamWriter file = new System.IO.StreamWriter(@filename))
            {
                Console.WriteLine("Writing file....");
                file.WriteLine("P1");
            
                //var letters = new Dictionary<String, String[]>();
                var letters = readLetters("lines.txt", 'B');
                //letter.Add("A", new String[] { "0 0 1 0 0" });
            
                file.WriteLine(word.Length+word.Length*5 + " " + 7);
                for (int i = 0; i < 7; i++)
                {
                    foreach (Char c in word)
                    {
                        //Console.Write(letters[c+""][i] + "0 ");
                        if (letters.ContainsKey(c + "")) 
                        {
                            file.Write(letters[c + ""][i] + "0 ");
                        }
                        else
                        {
                            Console.WriteLine("Unsuported character, writing failed!");
                            return;
                        }
                        
                    }
                    file.WriteLine();
                }
            }
            Console.WriteLine("PBM successfully writen.");
        }
    }
}
