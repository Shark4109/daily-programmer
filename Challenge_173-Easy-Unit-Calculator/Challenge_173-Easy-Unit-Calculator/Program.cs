﻿/*
 * You have a 30-centimetre ruler. Or is it a 11.8-inch ruler? Or is it even a 9.7-attoparsec ruler? It means the same 
 * thing, of course, but no-one can quite decide which one is the standard. To help people with this often-frustrating 
 * situation you've been tasked with creating a calculator to do the nasty conversion work for you.
 * 
 * Your calculator must be able to convert between metres, inches, miles and attoparsecs. It must also be able to convert 
 * between kilograms, pounds, ounces and hogsheads of Beryllium.
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Challenge_173_Easy_Unit_Calculator
{
    class Program
    {


        static void Main(string[] args)
        {
            var distance = new Dictionary<String, decimal>();
            distance.Add("meters", 1m);
            distance.Add("kilometers", 0.001m);
            distance.Add("inches", 39.3700787m);
            distance.Add("miles", 0.000621371192m);
            distance.Add("attoparsecs", 32.4077929m);

            var weight = new Dictionary<String, decimal>();
            weight.Add("kilograms", 1m);
            weight.Add("grams", 1000m);
            weight.Add("pounds", 2.20462262m);
            weight.Add("ounces", 35.2739619m);
            weight.Add("hogsheads", 32.4077929m);

            while (true)
            {
                Console.WriteLine("Please enter conversion:");
                string line = Console.ReadLine();
                if (line.ToLower().Equals("quit"))
                {
                    break;
                }
                string[] sLine = line.Split(' ');
                if (sLine.Length != 4)
                {
                    Console.WriteLine("Unsufficient number of parameters!");
                }
                else
                {
                    decimal d;
                    Decimal.TryParse(sLine[0], out d);
                    string from = sLine[1];
                    string to = sLine[3];

                    try
                    {
                        if (distance.ContainsKey(from))
                        {
                            Console.WriteLine(d + " " + from + " is " + string.Format("{0:0.00}", d * distance[to] / distance[from]) + " " + to);
                        }
                        else if (weight.ContainsKey(from))
                        {
                            Console.WriteLine(d + " " + from + " is " + string.Format("{0:0.00}", d * weight[to] / weight[from]) + " " + to);
                        }
                        else
                        {
                            Console.WriteLine("Cannot convert from " + from + " to " + to);
                        }
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("Cannot convert from " + from + " to " + to);
                    }

                }
            }
        }

    }
}
