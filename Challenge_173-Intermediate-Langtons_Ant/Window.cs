﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Timers;
using System.Threading;

namespace Challenge_173_Intermediate_Langtons_Ant
{
    public partial class Window : Form
    {
        private int counter;

        public Window()
        {
            InitializeComponent();
        }

        private void Window_Load(object sender, EventArgs e)
        {
            //Console.Write("test");
            System.Timers.Timer timerClock = new System.Timers.Timer();    
            timerClock.Elapsed += new ElapsedEventHandler(OnTimer);
            timerClock.Interval = 1;
            timerClock.Enabled = true;
            //this.Refresh();
        }


        public void OnTimer( Object source, ElapsedEventArgs e )
        {
            try
            {
                this.Invalidate();
            }
            catch (Exception ex)
            {
                
            }
            
        }
        protected override void OnPaint(PaintEventArgs e) 
        {
            //Graphics g = e.Graphics;
            Graphics g = this.CreateGraphics();
            Pen myPen = new Pen(System.Drawing.Color.Red, 5);
            
                //System.Threading.Thread.Sleep(50);
            g.DrawLine(myPen, 20, 20, counter,counter+10);
            counter++;
                           
        }

    }
}


